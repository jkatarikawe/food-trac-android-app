<?php

/**
 * @author FoodTrac
 * @copyright 2015
 */

class Producer_model extends CI_Model {
    
   
    
//Beginning Producer views
         public function get_prodbeef()
    {
        
        $this->db->order_by('customersub_time','DESC');
        $query = $this->db->get('p_beef');
        
        if ($query->num_rows() > 0)
        {
           return $query->result();
        }
        return false;
    }

     public function get_prodmaize()
    {
        
        $this->db->order_by('customersub_time','DESC');
        $query = $this->db->get('p_maizeflour');
        
        if ($query->num_rows() > 0)
        {
           return $query->result();
        }
        return false;
    }

     public function get_prodmatooke()
    {
        
        $this->db->order_by('customersub_time','DESC');
        $query = $this->db->get('p_matooke');
        
        if ($query->num_rows() > 0)
        {
           return $query->result();
        }
        return false;
    }

     public function get_prodmilk()
    {
        
        $this->db->order_by('customersub_time','DESC');
        $query = $this->db->get('p_milk');
        
        if ($query->num_rows() > 0)
        {
           return $query->result();
        }
        return false;
    }


   

}